using SnippetLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperLibrary;

namespace DomainModelGenerator
{
    public class Class1
    {
        [QueryHandler]
        public Class1 Get()
        {
            return this;
        }

        [CommandHandler]
        public void SetAThing(int i, List<string> t)
        {
        }

		///<snippet><generated>
		public void Class1SnipCaller()
		{
		}
		
		///<snippet><generated>
        [SnippetHandler]
        public void SetAThingPart2(int i, List<string> t)
        {
        }
    }
}